exec { 'Update centos':
  command    => 'yum clean all; yum -y update',
  path       => ['/usr/bin', '/usr/sbin',],
  timeout    => 0,
}

exec { 'Destroy containers':
  command    => 'dockerctl destroy all',
  path       => ['/usr/bin', '/usr/sbin',],
  timeout    => 0,
}

exec { 'Stop docker':
  command    => 'systemctl stop docker',
  path    => ['/usr/bin', '/usr/sbin',],
}

file { 'Clean docker config':
  path       => '/etc/sysconfig/docker-storage',
  ensure     => absent,
}

file { 'Clean docker files':
  path       => '/var/lib/docker/',
  ensure     => absent,
  # Use force as we remove a dir
  force      => true,
}

exec { 'Remove docker LVM':
  command    => 'udevadm settle; lvremove -f docker/docker-pool',
  path       => ['/usr/bin', '/usr/sbin',],
}

exec { 'Setup docker storage':
  command    => 'docker-storage-setup',
  path       => ['/usr/bin', '/usr/sbin',],
}

service{ 'Launch docker':
  name       => 'docker',
  ensure     => 'running',
}

exec { 'Start fuel':
  command    => 'docker load -i /var/www/nailgun/docker/images/fuel-images.tar; dockerctl start all',
  path       => ['/usr/bin', '/usr/sbin',],
  timeout    => 0,
}

Exec['Update centos'] -> Exec['Destroy containers'] -> Exec['Stop docker'] -> File['Clean docker config'] -> File['Clean docker files'] -> Exec['Remove docker LVM'] -> Exec['Setup docker storage'] -> Service['Launch docker'] -> Exec['Start fuel']
